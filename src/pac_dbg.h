#ifndef pac_dbg_h
#define pac_dbg_h

#include <assert.h>
#include <stdio.h>

extern bool FLAGS_pac_debug;

#define ASSERT(x)	assert(x)
#ifdef _MSC_VER
	#define DEBUG_MSG(...)	if ( FLAGS_pac_debug ) fprintf(stderr, __VA_ARGS__)
#else
	#define DEBUG_MSG(x...)	if ( FLAGS_pac_debug ) fprintf(stderr, x)
#endif

#endif /* pac_dbg_h */
