#include "pac_analyzer.h"
#include "pac_dataunit.h"
#include "pac_embedded.h"
#include "pac_exception.h"
#include "pac_expr.h"
#include "pac_flow.h"
#include "pac_output.h"
#include "pac_paramtype.h"
#include "pac_type.h"

#include "pac_conn.h"

ConnDecl::ConnDecl(ID *conn_id, 
                   ParamList *params,
                   AnalyzerElementList *elemlist)
	: AnalyzerDecl(conn_id, CONN, params)
	{
	flows_[0] = flows_[1] = 0;
	AddElements(elemlist);
	data_type_ = new ParameterizedType(conn_id->clone(), 0);
	}

ConnDecl::~ConnDecl()
	{
	delete flows_[0];
	delete flows_[1];
	}

void ConnDecl::AddBaseClass(vector<string> *base_classes) const
	{
	base_classes->push_back("binpac::ConnectionAnalyzer"); 
	} 

void ConnDecl::ProcessFlowElement(AnalyzerFlow *flow_elem)
	{
	int flow_index;

	if ( flow_elem->dir() == AnalyzerFlow::UP )
		flow_index = 0;
	else
		flow_index = 1;

	if ( flows_[flow_index] )
		{
		throw Exception(flow_elem,
		                fmt("%sflow already defined", 
		                    flow_index == 0 ? "up" : "down"));
		}

	flows_[flow_index] = flow_elem;
	type_->AddField(flow_elem->flow_field());
	}

void ConnDecl::ProcessDataUnitElement(AnalyzerDataUnit *dataunit_elem)
	{
	throw Exception(
		dataunit_elem, 
		"dataunit should be defined in only a flow declaration");
	}

void ConnDecl::Prepare()
	{
	AnalyzerDecl::Prepare();

	flows_[0]->flow_decl()->set_conn_decl(this);
	flows_[1]->flow_decl()->set_conn_decl(this);
	}

void ConnDecl::GenPubDecls(Output *out_h, Output *out_cc)
	{
	AnalyzerDecl::GenPubDecls(out_h, out_cc);
	}

void ConnDecl::GenPrivDecls(Output *out_h, Output *out_cc)
	{
	AnalyzerDecl::GenPrivDecls(out_h, out_cc);
	}

void ConnDecl::GenEOFFunc(Output *out_h, Output *out_cc)
	{
	string proto = strfmt("%s(bool is_orig)", kFlowEOF);

	out_h->println("void %s;", proto.c_str());

	out_cc->println("void %s::%s", class_name().c_str(), proto.c_str());
	out_cc->inc_indent();
	out_cc->println("{");

	out_cc->println("if ( is_orig )");
	out_cc->inc_indent();
	out_cc->println("%s->%s();",
	                env_->LValue(upflow_id),
	                kFlowEOF);
	out_cc->dec_indent();
	out_cc->println("else");
	out_cc->inc_indent();
	out_cc->println("%s->%s();",
	                env_->LValue(downflow_id),
	                kFlowEOF);

	foreach(i, AnalyzerHelperList, eof_helpers_)
		{
		(*i)->GenCode(0, out_cc, this);
		}

	out_cc->dec_indent();
	
	out_cc->println("}");
	out_cc->dec_indent();
	out_cc->println("");
	}

void ConnDecl::GenGapFunc(Output *out_h, Output *out_cc)
	{
	string proto = strfmt("%s(bool is_orig, int gap_length)", kFlowGap);

	out_h->println("void %s;", proto.c_str());

	out_cc->println("void %s::%s", class_name().c_str(), proto.c_str());
	out_cc->inc_indent();
	out_cc->println("{");

	out_cc->println("if ( is_orig )");
	out_cc->inc_indent();
	out_cc->println("%s->%s(gap_length);",
	                env_->LValue(upflow_id),
	                kFlowGap);
	out_cc->dec_indent();
	out_cc->println("else");
	out_cc->inc_indent();
	out_cc->println("%s->%s(gap_length);",
	                env_->LValue(downflow_id),
	                kFlowGap);
	out_cc->dec_indent();
	
	out_cc->println("}");
	out_cc->dec_indent();
	out_cc->println("");
	}
void ConnDecl::GenParseFunc(Output *out_h, Output *out_cc)
	{
		string proto = 
		strfmt("%s(bool is_orig, const_byteptr begin, const_byteptr end)",
			kParseFunc);
	out_h->println("Pac * %s;", proto.c_str());
	
	out_cc->println("Pac * %s::%s",  class_name().c_str(), proto.c_str());
	out_cc->inc_indent();
	out_cc->println("{");
	
	out_cc->println("__t_global_begin_of_data = begin;");
	
    out_cc->println("Pac * pac;");
	out_cc->println("try");
	out_cc->inc_indent();
	out_cc->println("{");
	out_cc->println("if ( is_orig )");
	out_cc->inc_indent();
	out_cc->println("pac = %s->%s(begin, end);",
	                env_->LValue(upflow_id),
	                kParseFunc);
	out_cc->dec_indent();
	out_cc->println("else");
	out_cc->inc_indent();
	out_cc->println("pac = %s->%s(begin, end);",
	                env_->LValue(downflow_id),
	                kParseFunc);
    out_cc->dec_indent();
    out_cc->println("%s->%s();",
                    env_->LValue(upflow_id),
	                kDeleteContextFunc);
    out_cc->println("%s->%s();",
                    env_->LValue(downflow_id),
	                kDeleteContextFunc);
    out_cc->println("}");
	out_cc->dec_indent();

	out_cc->println("catch ( binpac::Exception const &e )");
	out_cc->inc_indent();
	out_cc->println("{");
    out_cc->println("%s->%s();",
                    env_->LValue(upflow_id),
	                kDeleteDataUnitFunc);
    out_cc->println("%s->%s();",
                    env_->LValue(downflow_id),
	                kDeleteDataUnitFunc);                
    out_cc->println("%s->%s();",
                    env_->LValue(upflow_id),
	                kDeleteContextFunc);
    out_cc->println("%s->%s();",
                    env_->LValue(downflow_id),
	                kDeleteContextFunc);
      out_cc->println("throw;");
	out_cc->println("}");
	out_cc->dec_indent();
	
    out_cc->println("return pac;");
	
	/*
	out_cc->println("%s = new %s(%s);",
		env_->LValue(dataunit_id),
		dataunit_->data_type()->class_name().c_str(),
		dataunit_->data_type()->EvalParameters(out_cc, env_).c_str());
	
	out_cc->println("%s = new %s(%s);", 
		env_->LValue(analyzer_context_id),
		dataunit_->context_type()->class_name().c_str(),
		dataunit_->context_type()->EvalParameters(out_cc, env_).c_str());
		
	string parse_params = strfmt("%s, %s", 
		env_->RValue(begin_of_data), 
		env_->RValue(end_of_data)); 

	Type *unit_datatype = dataunit_->data_type();
	if ( RequiresAnalyzerContext::compute(unit_datatype) )
		{
		parse_params += ", ";
		parse_params += env_->RValue(analyzer_context_id);
		}
	out_cc->println("int t_dataunit__size;");
	out_cc->println("t_dataunit__size = dataunit_->Parse(%s);", parse_params.c_str());
	out_cc->println("return %s;", env_->LValue(dataunit_id));
	 */
	out_cc->println("}");
	out_cc->dec_indent();
	out_cc->println("");
	}
void ConnDecl::GenStoreFunc(Output *out_h, Output *out_cc)
	{
	string proto = 
		strfmt("%s(bool is_orig, const_byteptr begin, const_byteptr end, Pac * dataunit)",
			kStoreFunc);
	out_h->println("uint32 %s;", proto.c_str());

	out_cc->println("uint32 %s::%s", class_name().c_str(), proto.c_str());
	out_cc->inc_indent();
	out_cc->println("{");
	
    out_cc->println("int dataunit_size;");
	out_cc->println("if ( is_orig )");
	out_cc->inc_indent();
	out_cc->println("dataunit_size = %s->%s(begin, end, dataunit);",
	                env_->LValue(upflow_id),
	                kStoreFunc);
	out_cc->dec_indent();
	out_cc->println("else");
	out_cc->inc_indent();
	out_cc->println("dataunit_size = %s->%s(begin, end, dataunit);",
	                env_->LValue(downflow_id),
	                kStoreFunc);
	out_cc->dec_indent();
	out_cc->println("return dataunit_size;");
	
	out_cc->println("}");
	out_cc->dec_indent();
	out_cc->println("");
	}
void ConnDecl::GenProcessFunc(Output *out_h, Output *out_cc)
	{
	string proto = 
		strfmt("%s(bool is_orig, const_byteptr begin, const_byteptr end)",
		       kNewData);

	out_h->println("void %s;", proto.c_str());

	out_cc->println("void %s::%s", class_name().c_str(), proto.c_str());
	out_cc->inc_indent();
	out_cc->println("{");

	out_cc->println("if ( is_orig )");
	out_cc->inc_indent();
	out_cc->println("%s->%s(begin, end);",
	                env_->LValue(upflow_id),
	                kNewData);
	out_cc->dec_indent();
	out_cc->println("else");
	out_cc->inc_indent();
	out_cc->println("%s->%s(begin, end);",
	                env_->LValue(downflow_id),
	                kNewData);
	out_cc->dec_indent();
	
	out_cc->println("}");
	out_cc->dec_indent();
	out_cc->println("");
	}
