#
# A simple static wrapper for a number of standard Makefile targets,
# mostly just forwarding to build/Makefile. This is provided only for
# convenience and supports only a subset of what Ninja's Makefile
# to offer. For more, execute that one directly. 
#

BUILD=build
MAKE=ninja
REPO=`basename \`git config --get remote.origin.url | sed 's/^[^:]*://g'\``
VERSION_FULL=$(REPO)-`cat VERSION`
VERSION_MIN=$(REPO)-`cat VERSION`-minimal
HAVE_MODULES=git submodule | grep -v cmake >/dev/null
NINJA_EXIST := $(shell which ninja 2>/dev/null)
TAR_EXIST := $(shell which tar 2>/dev/null)
GIT_EXIST := $(shell which git 2>/dev/null)
 
all: configured
	$(MAKE) -C $(BUILD) $@

install: configured
	$(MAKE) -C $(BUILD) $@

clean: configured
	$(MAKE) -C $(BUILD) $@

dist: check-deps
	@rm -rf $(VERSION_FULL) $(VERSION_FULL).tgz
	@rm -rf $(VERSION_MIN) $(VERSION_MIN).tgz
	@git clone --recursive . $(VERSION_FULL) >/dev/null 2>&1
	@find $(VERSION_FULL) -name .git\* | xargs rm -rf
	@tar -czf $(VERSION_FULL).tgz $(VERSION_FULL) && echo Package: $(VERSION_FULL).tgz && rm -rf $(VERSION_FULL)
	@$(HAVE_MODULES) && git clone . $(VERSION_MIN) >/dev/null 2>&1 || exit 0
	@$(HAVE_MODULES) && (cd $(VERSION_MIN)) || exit 0
	@$(HAVE_MODULES) && find $(VERSION_MIN) -name .git\* | xargs rm -rf || exit 0
	@$(HAVE_MODULES) && tar -czf $(VERSION_MIN).tgz $(VERSION_MIN) && echo Package: $(VERSION_MIN).tgz && rm -rf $(VERSION_MIN) || exit 0

distclean:
	rm -rf $(BUILD)

.PHONY : configured check-deps
configured:
	@test -d $(BUILD) || ( echo "Error: No build/ directory found. Did you run configure?" && exit 1 )
	@test -e $(BUILD)/build.ninja || ( echo "Error: No build/ninja found. Did you run configure?" && exit 1 )
ifndef TAR_EXIST
	$(error "No ninja in $(PATH), consider doing install ninja")
endif

check-deps:
ifndef TAR_EXIST
	$(error "No tar in $(PATH), consider doing install tar")
endif
ifndef GIT_EXIST
	$(error "No git in $(PATH), consider doing install git")
endif
